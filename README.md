# Blague (@theplacelab/blague)

[![Semantic Release Badge](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![GitLab Pipeline Status](https://gitlab.com/theplacelab/packages/blague/badges/master/pipeline.svg)](https://gitlab.com/theplacelab/packages/blague/-/pipelines/latest)

☕ &nbsp; _Is this project helpful to you? [Please consider buying me a coffee](https://pay.feralresearch.org/tip)._

A bare-minimum, highly-opinionated, static-site generator written in Node.
You can see a [sample site](https://theplacelab.gitlab.io/packages/blague/) produced by this repo.

## Blague?

This is intended for blogging, it is also a joke. You should probably use [11ty instead](https://www.11ty.dev/).

## Installation

1. Add gitlab as a package resource, create a `.npmrc` as follows:
   `@theplacelab:registry=https://gitlab.com/api/v4/packages/npm/`

2. Add the package to your project:  
   `yarn add @theplacelab/blague`  
   After which you can run the site builder using `yarn blague` and watch for changes with `yarn blague --watch`

3. Optional: Add scripts to your package.json:

```
  "scripts": {
    "build": "yarn blague",
    "watch": "yarn blague --watch"
  },
```

4. Setup your configuration using `.blaguerc.json` (see below)

## Configuration

You can create an optional `.blaguerc.json` file. All options have defaults, the defaults will be overwritten by what's in this file.

```
  {
    "markdown": {
      "html": true,
      "linkify": true,
      "typographer": true
    },
    "directories": {
      "input": "src",
      "output": "build",
    }
  }
```

The "markdown" section [are the settings supported by markdown-it](https://github.com/markdown-it/markdown-it).

## Command Line Options

`--watch` watches the output directory for changes  
`--baseHref=URL` replaces the variable {baseHref} in templates, useful for hosting  
`--v` verbose logging

## Gitlab Pages

Sample .gitlab-ci.yaml

```
image: node

pages:
  script:
    - yarn install
    - yarn blague --v --baseHref=SITEURL
    - cp public/index.html public/404.html
  artifacts:
    paths:
      - public
```

## Configure source files

Source folder should have the following structure. See the EXAMPLE directory in this repo for working example, which is [also hosted here](https://theplacelab.gitlab.io/packages/blague/).

```
src/
├─ assets/
├─ pages/
├─ templates/
├─ partials/
```

## Does this handle....

Probably not.

## Wouldn't it be better to...

Probably.

## Can I...

This is MIT Licensed, go for it. If you do something fun with this, I'd like to hear about it, and if you want to make a PR, go for it.

## Tips

- You can write content in markdown or html or both (or mixed)
- The only dependency is [markdown-it](https://github.com/markdown-it/markdown-it), which supports many options and plugins.
- Content lives in `site`.
  - Everything in `public` will be copied to top level exactly as-is
  - Everything in `pages` will be run through the markdown processor, wrapped in a template and run through the partial resolver, the output will be a pile of .html files which follow the directory structure you specify.
  - Content is meant to be wrapped in a template which can refer to a partial. Partials are referenced with curly braces {partial}
  - It is possible to refer directly to a partial from any .html or .md file.
- You should always have at least a `default.html` in templates. All other templates are optional, blague will attempt to use the parent folder name as the name of the template to apply (so everything in `blog` will use `template/blog.html` and so on)
- If you provide an index file for a folder, that will be used. If you don't, the system will create an index listing.
- You can provide a date for a post by making sure the filename is prefaced with a JS datestamp (try Date.now() in your console to generate a new one).

## Live Preview

- If you run `nodemon` by itself in the root directory, any changes to content or the blague script will trigger a rebuild into the `build` directory. For preview, you can simultaneously run a server on this directory (this pairs nicely with [Browser Sync](https://browsersync.io/), I recommend `browser-sync start -s -f . --no-notify --host --port 9000`).

## TO FIX

- The version number that blague displays on run is always behind by one, because of the semantic-release build proces

- On dist build, throws a bunch of "critical dependency" warnings on yargs. See [this issue](https://github.com/yargs/yargs/issues/781).
