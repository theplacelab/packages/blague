const path = require("path");
const ShebangPlugin = require("webpack-shebang-plugin");

module.exports = {
  target: "node",
  entry: "./src/index.js",
  plugins: [new ShebangPlugin()],
  module: {
    rules: [
      {
        test: /.node$/,
        loader: "node-loader"
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
    ]
  },
  output: {
    path: path.join(__dirname, "dist"),
    filename: "main.js"
  }
};
