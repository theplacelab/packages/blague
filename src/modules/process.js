const fs = require("fs");
const path = require("path");
const { DateTime } = require("luxon");

const globalReplace = (config, content) => {
  const dt = DateTime.now();
  return content
    .replaceAll(`{baseHref}`, config.site.baseHref)
    .replaceAll(`{buildDate}`, dt.toLocaleString(DateTime.DATETIME_MED));
};

module.exports = {
  mergeWithPartials: (config, text) => {
    let result = text;
    const partials = text.match(/{(.*?)}/gm);
    partials?.forEach((partial) => {
      const partialFile = `${config.directories.input}/partials/${partial
        .replaceAll("{", "")
        .replaceAll("}", "")
        .toLowerCase()}.html`;
      let partialContents = "";
      if (fs.existsSync(partialFile)) {
        partialContents = fs.readFileSync(partialFile, "utf8");
      } else {
        if (partialFile.split(" ").length === 0)
          console.log(`[ 🟡 ]: ...Missing partial ${partialFile}`);
      }
      result = result.replaceAll(partial, partialContents);
    });
    return globalReplace(config, result);
  },

  createIndexIfNeeded: async (config, dir) => {
    const md = require("markdown-it")(config.markdown);
    const indexFile = `${dir.replace(
      `${config.directories.input}/pages`,
      `${config.directories.output}`
    )}/index.html`;

    const generateLink = (file) => {
      let text, url;
      let fileSplit = file.split("-");
      postDate = new Date(parseInt(fileSplit[0])).toLocaleDateString();
      if (postDate === "Invalid Date") {
        postDate = null;
        text = file.split("-").join(" ").replaceAll(".html", "");
        url = file;
      } else {
        fileSplit.shift();
        text = `${fileSplit
          .join(" ")
          .replaceAll(".html", "")
          .toLowerCase()
          .replace(/(^\w|\s\w)/g, (m) => m.toUpperCase())}`;
        const arr = file.split("-");
        arr.shift();
        url = arr.join("-");
      }
      return `${
        postDate ? `${postDate}<br/>` : ""
      }<a href="${url}">${text}</a>`;
    };

    if (!fs.existsSync(indexFile)) {
      console.log(`[ 🛠  ]: ...INDEX: ${indexFile.replace(__dirname, "")}`);
      let content = `<h1>${dir
        .replace(`${config.directories.input}/pages`, "")
        .replace("/", "")
        .replace(/(^\w|\s\w)/g, (m) => m.toUpperCase())}</h1>`;
      content += "<ul>";
      const files = await fs.promises.readdir(dir);
      for (const file of files) {
        const srcPath = path.join(dir, file);
        const stat = await fs.promises.stat(srcPath);
        const htmlFile = `${file.split(".")[0]}.html`;
        if (stat.isFile()) {
          content += `<li>${generateLink(htmlFile)}</li>`;
        }
      }
      content += "</ul>";

      const template = fs.readFileSync(
        `${`${config.directories.input}/templates`}/default.html`,
        "utf8"
      );

      fs.writeFile(
        indexFile,
        module.exports.mergeWithPartials(
          config,
          template.replace("{content}", md.render(content))
        ),
        () => {}
      );
    }
  },

  processPage: (config, inputFile) => {
    const md = require("markdown-it")(config.markdown);
    const ext = path.extname(inputFile.toLowerCase()).split(".").pop();
    switch (ext) {
      case "html":
      case "md": {
        const pathArr = inputFile
          .replace(`${config.directories.input}/pages`, "")
          .split("/");
        let templateName = pathArr[pathArr.length - 2];
        templateName = templateName ? templateName : "default";
        const content = fs.readFileSync(inputFile, "utf8");
        if (content) {
          if (
            !fs.existsSync(
              `${`${config.directories.input}/templates`}/${templateName}.html`
            )
          ) {
            templateName = "default";
          }
          template = fs.readFileSync(
            `${`${config.directories.input}/templates`}/${templateName}.html`,
            "utf8"
          );
          const html = template.replace("{content}", md.render(content));
          let filePath = inputFile
            .replace(`${config.directories.input}/pages`, "")
            .split(".")[0];
          let fileName = filePath.split("/")[filePath.split("/").length - 1];
          const postDate = new Date(
            parseInt(fileName.split("-")[0])
          ).toLocaleDateString();
          if (postDate !== "Invalid Date") {
            fileName = filePath.split("/").pop();
            const restOfPath = filePath.replace(fileName, "");
            fileName = fileName.split("-");
            fileName.shift();
            fileName = fileName.join("-");
            filePath = `${restOfPath}${fileName}`;
          }
          const outputFile = `${`${config.directories.output}`}/${filePath}.html`;
          console.log(
            `[ 🟢 ]: ...${ext} ⇒ ${templateName} ⇒ ${outputFile.replace(
              `${`${config.directories.output}`}/`,
              ""
            )}`
          );
          fs.writeFile(
            outputFile,
            module.exports.mergeWithPartials(
              config,
              globalReplace(config, html)
            ),
            () => {}
          );
        }
        break;
      }
      default:
        console.log(
          `[ 🟡 ]: ...Skip: ${inputFile.replace(
            `${config.directories.input}/pages`,
            ""
          )}`
        );
        break;
    }
  }
};
