const fs = require("fs");
module.exports = {
  default: {
    site: {
      baseHref: ""
    },
    markdown: {
      html: true,
      linkify: true,
      typographer: true
    },
    directories: {
      build: `build`,
      source: `src`
    }
  },
  applyConfigFile: (argv, conf = `.blaguerc.json`) => {
    let config = fs.existsSync(conf)
      ? JSON.parse(fs.readFileSync(conf, "utf-8"))
      : {};
    console.log(
      `[ 🪛  ]: CONFIG: ${
        fs.existsSync(conf) ? conf : `${conf} does not exist (using defaults)!`
      }`
    );
    let configuration = {
      site: {
        ...module.exports.default.site,
        ...config.site
      },
      markdown: {
        ...module.exports.default.markdown,
        ...config.markdown
      },
      directories: {
        ...module.exports.default.directories,
        ...config.directories
      }
    };
    // Command line overrides
    configuration =
      argv.baseHref?.length > 0
        ? {
            ...configuration,
            site: { ...configuration.site, baseHref: argv.baseHref }
          }
        : configuration;
    if (argv.v) console.log(configuration);
    return configuration;
  }
};
