const fs = require("fs");
const path = require("path");
module.exports = {
  makeDir: (dir, cb) => {
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir, { recursive: true });
      if (typeof cb === "function") cb();
    } else {
      if (typeof cb === "function") cb();
    }
  },
  traverse: async ({ recurse = false, src, onFile, onDir }) => {
    try {
      const files = await fs.promises.readdir(src);
      for (const file of files) {
        const srcPath = path.join(src, file);
        const stat = await fs.promises.stat(srcPath);
        if (stat.isFile()) {
          if (typeof onFile === "function") onFile(srcPath);
        } else if (stat.isDirectory()) {
          if (typeof onDir === "function") {
            onDir(srcPath);
            if (recurse)
              module.exports.traverse({ recurse, src: srcPath, onFile, onDir });
          }
        }
      }
    } catch (e) {
      console.error("[ 🚨 ]: ", e);
    }
  },
  debounce: (callback, wait) => {
    // https://www.joshwcomeau.com/snippets/javascript/debounce/
    let timeoutId = null;
    return (...args) => {
      global.clearTimeout(timeoutId);
      timeoutId = global.setTimeout(() => {
        callback.apply(null, args);
      }, wait);
    };
  }
};
