const breakPoint = 845;
let burgerOpen = false;

window.addEventListener("load", () => {
  document.body.style.opacity = 1;
  burgerOpen = window.innerWidth <= breakPoint;

  const installDownloadAllButton = () => {
    const galleries = document.getElementsByClassName("gallery");
    if (galleries)
      Array.from(galleries).forEach((gallery) => {
        const galleryActions = document.createElement("div");
        galleryActions.className = "galleryActions";
        const spinner = document.createElement("div");
        spinner.className = "spinner";
        const button = document.createElement("div");
        button.className = "galleryActionButton";
        button.innerHTML =
          '<svg viewBox="11 5 10 25" aria-hidden="true"  width="25px" height="25px"><path stroke-width="0" fill="black" d="M20.5 14.3 17.1 18V10h-2.2v7.9l-3.4-3.6L10 16l6 6.1 6-6.1-1.5-1.6ZM23 23H9v2h14"></path></svg>';
        let isWorking = false;
        button.addEventListener("click", async () => {
          if (isWorking) return;
          isWorking = true;
          const images = gallery.getElementsByTagName("a");
          let files = [];
          button.style.opacity = 0.5;
          button.appendChild(spinner);
          for (let el of images) {
            let blobUrl = el.href;
            let fileName = blobUrl.split("/").pop();
            let file = await fetch(blobUrl)
              .then((r) => r.blob())
              .then(
                (blobFile) =>
                  new File([blobFile], fileName, { type: "image/png" })
              );
            files.push(file);
          }
          const blob = await downloadZip(files).blob();
          const link = document.createElement("a");
          link.href = URL.createObjectURL(blob);
          link.download = "gallery.zip";
          link.click();
          link.remove();
          URL.revokeObjectURL(link.href);
          spinner.remove();
          button.style.opacity = 1;
          isWorking = false;
        });
        galleryActions.appendChild(button);
        if (gallery) gallery.prepend(galleryActions);
      });
  };
  installDownloadAllButton();
});

const toggleBurger = () => {
  if (window.innerWidth > breakPoint) return;
  burgerOpen = !burgerOpen;
  document.getElementById("navbar").style.display = burgerOpen
    ? "none"
    : "flex";
};

const downloadBlob = (blob, name) => {
  const blobUrl = URL.createObjectURL(blob);
  const link = document.createElement("a");
  link.href = blobUrl;
  link.download = name;
  document.body.appendChild(link);
  link.dispatchEvent(
    new MouseEvent("click", {
      bubbles: true,
      cancelable: true,
      view: window
    })
  );
  document.body.removeChild(link);
};
